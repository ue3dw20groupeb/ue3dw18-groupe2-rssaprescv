<?php

namespace Watson\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class RSSController {

    /**
     * RSS page controller.
     *
     * @param Application $app Silex application
     */
    public function indexAction(Application $app) {
        $links = $app['dao.link']->findAll();
		$links = array_slice($links, 0, 15);
        return $app['twig']->render('rss.html.twig', array('links' => $links));
    }

}
